package mcfg

import (
	"gopkg.in/yaml.v3"
	"strings"
)

func (auth Auth) MarshalYAML() (interface{}, error) {
	crypted, err := GetAESEncrypted(auth.IDToken)

	if err != nil {
		return nil, err
	}
	auth.IDToken = "CRYPT:" + crypted

	crypted, err = GetAESEncrypted(auth.Token)
	if err != nil {
		return nil, err
	}
	auth.Token = "CRYPT:" + crypted

	type alias Auth
	node := yaml.Node{}
	err = node.Encode(alias(auth))
	if err != nil {
		return nil, err
	}

	// This comment WILL be included in the output
	node.HeadComment = "Some of these keys are encrypted with the password\nyou gave. Do not modify this file manually."
	return node, nil
}

func (auth *Auth) UnmarshalYAML(node *yaml.Node) error {
	type alias Auth

	dataAlias := (*alias)(auth)

	err := node.Decode(&dataAlias)
	if err != nil {
		return err
	}

	idt := strings.Split(auth.IDToken, ":")
	if idt[0] == "CRYPT" {
		auth.IDToken, err = GetAESDecrypted(idt[1])
		if err != nil {
			return err
		}
	}

	tok := strings.Split(auth.Token, ":")
	if tok[0] == "CRYPT" {
		auth.Token, err = GetAESDecrypted(tok[1])
		if err != nil {
			return err
		}
	}

	return nil
}
