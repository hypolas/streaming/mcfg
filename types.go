package mcfg // import gitlab.com/hypolas/streaming/mcfg/types

type Cfg struct {
	Services map[string]*Service `yaml:"services"`
}

type Service struct {
	Auth Auth `yaml:"auth"`
}

type Auth struct {
	Token    string `yaml:"token"`
	IDToken  string `yaml:"id_token"`
	UserName string `yaml:"username"`
	ClientID string `yaml:"clientID"`
}

type CryptOptions struct {
	InputText string
	Key       string
}

type KeyConf struct {
	Key string
}

var keyConf KeyConf
