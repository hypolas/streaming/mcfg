package mcfg

import (
	"os"
	"strings"

	"github.com/google/uuid"
	"gopkg.in/yaml.v3"
)

// PKCS5UnPadding  pads a certain blob of data with necessary data to be used in AES block cipher
func PKCS5UnPadding(src []byte) []byte {
	length := len(src)
	unpadding := int(src[length-1])

	return src[:(length - unpadding)]
}

func ExpendKey(key string) (newKey string) {
	return strings.ReplaceAll(uuid.NewSHA1(uuid.NameSpaceX500, []byte(key)).String(), "-", "")
}

func (cfg *Cfg) CfgWriter(filePath string, cfgKey KeyConf) error {
	keyConf = cfgKey
	f, err := yaml.Marshal(cfg)
	if err != nil {
		return err
	}

	os.WriteFile(filePath, f, os.ModePerm)

	return nil
}

func CfgReader(filePath string, cfgKey KeyConf) (Cfg, error) {
	keyConf = cfgKey
	r, err := os.ReadFile(filePath)
	if err != nil {
		return Cfg{}, err
	}

	c := Cfg{}
	yaml.Unmarshal(r, &c)

	return c, nil
}
