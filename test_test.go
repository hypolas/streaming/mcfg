package mcfg

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEncrypt(t *testing.T) {
	fmt.Println("########## Start TestEncrypt ##############")
	plainText := "Hello, World!"
	fmt.Println("This is an original:", plainText)

	encrypted, err := GetAESEncrypted(plainText)

	if err != nil {
		fmt.Println("Error during encryption", err)
	}

	fmt.Println("This is an encrypted:", encrypted)
	assert.NotEqual(t, plainText, encrypted, "The two words should not be the same.")

	decrypted, err := GetAESDecrypted(encrypted)

	if err != nil {
		fmt.Println("Error during decryption", err)
	}

	assert.Equal(t, plainText, string(decrypted), "The two words should be the same.")

	fmt.Println("############ End TestEncrypt ##############")
}

func TestWriteCfg(t *testing.T) {
	fmt.Println("########## Start TestWriteCfg ##############")
	key := KeyConf{}
	key.Key = "tefl!mksjzgolkfgjnopzirjgnifqsnbcpiznbizrgpifdjngporijgozrgzrgfsdgst"
	cff, err := CfgReader("test/secret.yml", key)
	if err != nil {
		panic(err)
	}

	val1 := cff.Services["twitch"].Auth.Token

	cff.CfgWriter("test/secret_byFunc.yml", key)

	val2 := cff.Services["twitch"].Auth.Token
	assert.Equal(t, val2, val1, "The two words should be the same.")
}
