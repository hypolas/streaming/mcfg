module gitlab.com/hypolas/streaming/mcfg

go 1.21.4

require (
	github.com/Adeithe/go-twitch v0.2.3
	github.com/google/uuid v1.4.0
	github.com/stretchr/testify v1.8.4
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
